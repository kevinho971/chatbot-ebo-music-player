import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ShareItemModel } from './shareItem.model';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { CommentInterface } from '../../models/comment.interface';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(private http: HttpClient) {}

  configUrl = environment.urlShareItem;

  getShareItem(params: HttpParams) {
    return this.http
      .get<ShareItemModel>(this.configUrl + '/shareitem', { params: params });
  }
}
