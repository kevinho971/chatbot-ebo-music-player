import { Component, HostListener, Inject, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MusicPlayerService } from 'ngx-soundmanager2plus';
import { DataService } from '../../services/data/data.service';
import { ShareItemModel } from '../../services/playlist/shareItem.model';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { SpinnerVisibilityService } from 'ng-http-loader';
import { faVolumeMute } from '@fortawesome/free-solid-svg-icons/faVolumeMute';

declare const soundManager: any;

@Component({
  selector: 'ebo-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerComponent implements OnInit, OnDestroy {
  @Input()
  songs: Array<any>;
  @Input()
  shareItem: ShareItemModel;

  @Input()
  play = false;
  mute: boolean;

  currentPlaying: any;

  currentTrackPosition: number;
  currentTrackDuration: number;
  currentTrackProgress: number;
  volume: number;

  // subscriptions
  private _musicPlayerMuteSubscription: any;
  private _musicPlayerTrackIdSubscription: any;
  private _musicPlayerVolumeSubscription: any;

  state: boolean;
  isRepeat: boolean;
  isShuffle: boolean;

  previousButton;
  nextButton;

  volumeMute = faVolumeMute;

  fixed: boolean;
  fixedDesktop: boolean;

  @HostListener('window:scroll', [])
  checkScroll() {
    const scrollPosition = window.pageYOffset;
    this.fixed = scrollPosition >= 700;
    this.fixedDesktop = scrollPosition >= 320;
  }

  constructor(
    private _musicPlayerService: MusicPlayerService,
    private data: DataService,
    private titleService: Title,
    private spinner: SpinnerVisibilityService,
    @Inject(DOCUMENT) private document
  ) {}

  ngOnInit() {
    this.soundMananagerSetup();

    this.subscribe();

    if (this.play) {
      this.state = true;
    }

    setTimeout(() => {
      this.disableOnInit();
    }, 500);
  }

  soundMananagerSetup() {
    soundManager.setup({
      debugMode: false,
      defaultOptions: {
        useHighPerformance: true,
        stream: false,
        onfinish: () => {
          this._musicPlayerService.nextTrack();
          this.stateTrueNext();
        },
        onload: () => {
          setTimeout(() => {
            this.spinner.hide();
          }, 100);
        },
        onplay: () => {
          this.spinner.show();
        }
      }
    });
  }

  subscribe() {
    this.data.currentPlay.subscribe(state => (this.state = state));
    this.data.currentRepeat.subscribe(isRepeat => (this.isRepeat = isRepeat));
    this.data.currentShuffle.subscribe(isShuffle => (this.isShuffle = isShuffle));

    // Subscribe for mute changes to update bindings
    this.mute = this._musicPlayerService.getMuteStatus();
    this._musicPlayerMuteSubscription = this._musicPlayerService.musicPlayerMuteEventEmitter.subscribe((event: any) => {
      this.mute = event.data;
    });

    // Subscribe for track changes
    this.currentPlaying = this._musicPlayerService.currentTrackData();
    this._musicPlayerTrackIdSubscription = this._musicPlayerService.musicPlayerTrackEventEmitter.subscribe(
      (event: any) => {
        this.currentPlaying = this._musicPlayerService.currentTrackData();
        this.currentTrackPosition = this._musicPlayerService.position;
        this.currentTrackDuration = event.data.trackDuration;
        this.currentTrackProgress = event.data.trackProgress;
      }
    );

    // subscribe for volume changes
    this.volume = this._musicPlayerService.getVolume();
    this._musicPlayerVolumeSubscription = this._musicPlayerService.musicPlayerVolumeEventEmitter.subscribe(
      (event: any) => {
        this.volume = event.data;
        this._musicPlayerService.volume = this.volume;
      }
    );
  }

  disableOnInit() {
    //disable next/previous button on init
    this.previousButton = document.querySelector('button.previous');
    this.nextButton = document.querySelector('button.next');
    this.previousButton.setAttribute('disabled', '');
    this.nextButton.setAttribute('disabled', '');
  }

  stateTruePrevious() {
    this.resetProgress();
    this.previousButton = document.querySelector('button.previous');
    this.nextButton = document.querySelector('button.next');
    if (this.currentPlaying !== this.songs[0]) {
      this.state = true;
    } else if (this.currentPlaying === this.songs[0]) {
      this.state = true;
      this.previousButton.setAttribute('disabled', '');
    }
    if (this.nextButton) {
      this.nextButton.removeAttribute('disabled', '');
    }
    this.titleService.setTitle(this.currentPlaying.title + ' - ' + this.currentPlaying.artist + ' | Ebo Music Player');
  }

  stateTrueNext() {
    this.previousButton = document.querySelector('button.previous');
    this.nextButton = document.querySelector('button.next');
    if (this.previousButton) {
      this.previousButton.removeAttribute('disabled', '');
    }
    if (this.currentPlaying === this.songs[0]) {
      this.previousButton.setAttribute('disabled', '');
    }
    if (
      this.currentPlaying === this.songs[this.songs.length - 1] &&
      this.isRepeat === false &&
      this.isShuffle === false
    ) {
      this.nextButton.setAttribute('disabled', '');
    } else {
      if (this.nextButton) {
        this.nextButton.removeAttribute('disabled', '');
      }
    }
    this.resetProgress();
    this.state = true;
    this.titleService.setTitle(this.currentPlaying.title + ' - ' + this.currentPlaying.artist + ' | Ebo Music Player');
  }

  pauseSong() {
    this.data.changePlay(false);
  }

  playSong() {
    if (this.currentPlaying !== this.songs[this.songs.length - 1]) {
      this.nextButton.removeAttribute('disabled', '');
    }
    this.titleService.setTitle(this.currentPlaying.title + ' - ' + this.currentPlaying.artist + ' | Ebo Music Player');
    this.data.changePlay(true);
  }

  toggleMute() {
    this.mute = !this.mute;
    this._musicPlayerService.mute();
  }

  //Function servant à reset la barre de progression
  resetProgress() {
    this._musicPlayerService.adjustProgress(0);
  }

  // Récupération barre de progression
  get progress() {
    if (this.currentPlaying === undefined) {
      return 0;
    }
    return this.currentTrackPosition / this.currentPlaying.duration / 10;
  }

  // Toggle isRepeat
  repeat() {
    this.isRepeat = !this.isRepeat;
    this.data.changeRepeat(this.isRepeat);
  }

  // Toggle isShuffle
  shuffle() {
    this.isShuffle = !this.isShuffle;
    this.data.changeShuffle(this.isShuffle);
  }

  getVolumeSlider(event: any) {
    if (this.mute) {
      this._musicPlayerService.mute();
    }
    this._musicPlayerService.adjustVolumeSlider(event.value);
  }

  ngOnDestroy() {
    this._musicPlayerMuteSubscription.unsubscribe();
    this._musicPlayerTrackIdSubscription.unsubscribe();
    this._musicPlayerVolumeSubscription.unsubscribe();
  }
}
