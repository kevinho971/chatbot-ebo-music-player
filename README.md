# chatbot Ebo Music Player

Hello Ebo Team,

Here is my assignment.

I made an application related to my actual job.

## Getting started

To launch the project : 

- npm install
- npm run delete-ngx-module
- ng serve


I hope you will enjoy it !

## Command list

- Hello
- Play
- Pause
- Next track
- Previous track
- Shuffle / Remove Shuffle
- Stop
- Increase volume 
- Decrease volume
- Loop / Unloop
- Mute


### Kevin Morand
