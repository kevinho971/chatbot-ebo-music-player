import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { CommentInterface } from '../../models/comment.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private isPlay = new BehaviorSubject<boolean>(false);
  currentPlay = this.isPlay.asObservable();

  private repeat = new BehaviorSubject<boolean>(false);
  currentRepeat = this.repeat.asObservable();

  private shuffle = new BehaviorSubject<boolean>(false);
  currentShuffle = this.shuffle.asObservable();

  constructor() {}

  changePlay(state: boolean) {
    this.isPlay.next(state);
  }

  changeRepeat(isRepeat: boolean) {
    this.repeat.next(isRepeat);
  }

  changeShuffle(isShuffle: boolean) {
    this.shuffle.next(isShuffle);
  }
}
