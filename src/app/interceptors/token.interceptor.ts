import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (
      request.method === 'GET' ||
      request.method === 'DELETE' ||
      request.method === 'POST'
    ) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${environment.witToken}`
        }
      });
    } else if (request.method === 'PUT') {
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${environment.witToken}`
        }
      });
    }
    return next.handle(request).pipe(
      tap(
        event => this.handleResponse(request, event),
        error => this.handleError(request, error),
      )
    );
  }

  handleResponse(req: HttpRequest<any>, event: HttpEvent<any>) {
    // console.log('Handling response for ', req.url, event);
    if (event instanceof HttpResponse) {
      // console.log('Request for ', req.url,
      //   ' Response Status ', event.status,
      //   ' With body ', event.body);
    }
  }

  handleError(req: HttpRequest<any>, event: { status: any; error: any; }) {
    // console.error('Request for ', req.url,
    //   ' Response Status ', event.status,
    //   ' With error ', event.error);
  }
}
