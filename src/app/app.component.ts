import { Component, OnInit } from '@angular/core';
import { PlaylistService } from './services/playlist/playlist.service';
import { Title } from '@angular/platform-browser';
import { HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeJa from '@angular/common/locales/ja';


@Component({
  selector: 'ebo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Ebo Music Player';
  constructor(private playlistService: PlaylistService,
              private titleService: Title,
              private _translateService: TranslateService) {}

  ngOnInit() {
    if (this._translateService.getBrowserLang() === 'fr') {
      registerLocaleData(localeFr, 'fr');
    } else if (this._translateService.getBrowserLang() === 'ja') {
      registerLocaleData(localeJa, 'ja');
    }
    this._translateService.setDefaultLang('en');
    this._translateService.use(navigator.language.slice(0, 2));
  }
}
