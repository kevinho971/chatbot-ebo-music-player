import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { PlaylistService } from '../../services/playlist/playlist.service';
import { MusicPlayerService } from 'ngx-soundmanager2plus';
import { DataService } from '../../services/data/data.service';
import { ShareItemModel } from '../../services/playlist/shareItem.model';
import { Title } from '@angular/platform-browser';
import { SnackbarService } from '../../services/snackbar.service';

@Component({
  selector: 'ebo-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit {
  constructor(
    private playlistService: PlaylistService,
    private _musicPlayerService: MusicPlayerService,
    private data: DataService,
    private titleService: Title,
    private _snackbarService: SnackbarService
  ) {}
  @Input()
  songs: Array<any>;
  @Input()
  shareItem: ShareItemModel;

  @Output()
  likeDislikeAction = new EventEmitter<boolean>(false);

  currentPlaying: any;

  currentTrackPosition: number;
  currentTrackDuration: number;
  currentTrackProgress: number;

  private _musicPlayerTrackIdSubscription: any;

  state: boolean;
  isRepeat: boolean;
  isShuffle: boolean;

  previousButton;
  nextButton;

  fixed;

  isShow = false;

  @HostListener('window:scroll', [])
  checkScroll() {
    const scrollPosition = window.pageYOffset;
    this.fixed = scrollPosition >= 700;
  }

  ngOnInit() {
    this.subscribe();
  }

  subscribe() {
    this.data.currentPlay.subscribe(state => (this.state = state));
    this.data.currentRepeat.subscribe(isRepeat => (this.isRepeat = isRepeat));
    this.data.currentShuffle.subscribe(isShuffle => (this.isShuffle = isShuffle));

    this.currentPlaying = this._musicPlayerService.currentTrackData();
    this._musicPlayerTrackIdSubscription = this._musicPlayerService.musicPlayerTrackEventEmitter.subscribe(
      (event: any) => {
        this.currentPlaying = this._musicPlayerService.currentTrackData();
        this.currentTrackPosition = event.data.trackPosition;
        this.currentTrackDuration = event.data.trackDuration;
        this.currentTrackProgress = event.data.trackProgress;
      }
    );
  }

  resetProgress() {
    this._musicPlayerService.adjustProgress(0);
  }

  playSong() {
    this.previousButton = document.querySelector('button.previous');
    this.nextButton = document.querySelector('button.next');
    if (this.currentPlaying === this.songs[0] && this.isShuffle === false) {
      this.previousButton.setAttribute('disabled', '');
    } else {
      if (this.previousButton) {
        this.previousButton.removeAttribute('disabled', '');
      }
    }
    if (
      this.currentPlaying === this.songs[this.songs.length - 1] &&
      this.isRepeat === false &&
      this.isShuffle === false
    ) {
      this.nextButton.setAttribute('disabled', '');
    } else {
      if (this.nextButton) {
        this.nextButton.removeAttribute('disabled', '');
      }
    }
    this.titleService.setTitle(this.currentPlaying.title + ' - ' + this.currentPlaying.artist + ' | Ebo Music Player');
    this.resetProgress();
    this.data.changePlay(true);
  }
}
