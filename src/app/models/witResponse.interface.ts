export interface Intent {
  id: string;
  name: string;
  confidence: number;
}

export interface Entities {
}

export interface Traits {
}

export interface WitResponse {
  text: string;
  intents: Intent[];
  entities: Entities;
  traits: Traits;
}
