export interface Client2 {
  id: string;
  name: string;
}

export interface Catalog {
  id: string;
  name: string;
}

export interface CustomerAttributes {
  id: string;
}

export interface Country {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  abbreviation: string;
  name: string;
}

export interface Address {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  firstName: string;
  lastName: string;
  emailAddress: string;
  companyName: string;
  addressLine1: string;
  addressLine2?: any;
  addressLine3?: any;
  city: string;
  country: Country;
  postalCode: string;
  zipCode?: any;
  phonePrimary?: any;
  phoneSecondary?: any;
  phoneFax?: any;
  isDefault: boolean;
  active: boolean;
  business: boolean;
  default: boolean;
}

export interface CustomerAddress {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  addressName: string;
  address: Address;
  tvaIntracommunautaire?: any;
  orderType: string;
  code?: any;
}

export interface Customer {
  id: string;
  creationDate: number;
  updateDate: number;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  client: Client2;
  catalog: Catalog;
  username: string;
  password: string;
  emailAddress?: any;
  companyName: string;
  firstName?: any;
  lastName?: any;
  customerSubscription?: any;
  customerAttributes: CustomerAttributes;
  customerAddresses: CustomerAddress[];
  customerPhones: any[];
  code?: any;
  fullName: string;
}

export interface Client {
  id: string;
  creationDate: number;
  updateDate: number;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  bid: string;
  name: string;
  www: string;
  defaultAddress?: any;
  customer: Customer;
  timeZone: string;
  facebookWall?: any;
  active: boolean;
}

export interface Artist {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: number;
  name: string;
}

export interface Album {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: number;
  title: string;
  artist: Artist;
}

export interface Artist2 {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: number;
  name: string;
}

export interface Media {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  type: string;
  title: string;
  duration: number;
  size: number;
  fileChecksum: string;
  frequency: number;
  bitrate: number;
  track: string;
  album: Album;
  artist: Artist2;
  newMedia: boolean;
  xid: string;
  copyright: string;
  gain: number;
  gainbatch: number;
  fileDate: any;
  tags: any[];
  formatedDuration: string;
  fullTitle: string;
}

export interface AssoMediaPlaylist {
  id: string;
  creationDate: any;
  updateDate: any;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  displayOrder: number;
  media: Media;
  tags: any[];
  playlistHistory?: any;
  notifyWhenPlayed: boolean;
  doNotPlayAfter?: any;
  doNotPlayBefore?: any;
  boostVolume: number;
}

export interface Playlist {
  id: string;
  creationDate: number;
  updateDate: number;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  title: string;
  client: Client;
  current: boolean;
  assoMediaPlaylist: AssoMediaPlaylist[];
  preferedBitrate: string;
  state: string;
  version: number;
  size: number;
  fullName: string;
  formatedDuration: string;
  duration: number;
  mediaCount: number;
}

export interface ShareItemModel {
  id: string;
  creationDate: number;
  updateDate: number;
  creationUser: string;
  updateUser: string;
  remoteUpdateDate?: any;
  localUpdateDate?: any;
  name: string;
  shareItemUrl: string;
  description: string;
  playlist: Playlist;
  privateShareItem: boolean;
}
