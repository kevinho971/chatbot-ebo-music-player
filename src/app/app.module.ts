import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NgxSoundmanager2PlusModule } from 'ngx-soundmanager2plus';

import { AppComponent } from './app.component';
import { MinutesSecondsPipe } from './pipes/minutes-seconds.pipe';
import { HomeComponent } from './components/home/home.component';
import { PlayerComponent } from './components/player/player.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { DataService } from './services/data/data.service';
import { RepeatDirective } from './directives/repeat.directive';
import { ShuffleDirective } from './directives/shuffle.directive';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSliderModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgPipesModule } from 'ngx-pipes';
import { ShortenPipe } from './pipes/shorten.pipe';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LanguageService } from './services/language.service';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { TokenInterceptor } from './interceptors/token.interceptor';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: '**',
    redirectTo: '',
    component: HomeComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MinutesSecondsPipe,
    HomeComponent,
    PlayerComponent,
    PlaylistComponent,
    RepeatDirective,
    ShuffleDirective,
    ShortenPipe,
    ChatbotComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgHttpLoaderModule,
    FontAwesomeModule,
    NgxSoundmanager2PlusModule.forRoot(),
    NgxPageScrollModule,
    MatSliderModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, scrollPositionRestoration: 'enabled' } // <-- debugging purposes only
    ),
    BrowserAnimationsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    NgPipesModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatMenuModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [MinutesSecondsPipe, DataService, RepeatDirective,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: LOCALE_ID,
      deps: [LanguageService],
      useFactory: (languagesService) => languagesService.getLanguage(),
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

