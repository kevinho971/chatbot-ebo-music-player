import { Directive, HostListener, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { MusicPlayerService } from 'ngx-soundmanager2plus';

@Directive({
  selector: '[appRepeat]'
})
export class RepeatDirective implements OnInit, OnDestroy {
  repeat: boolean;

  // subscriptions
  private _musicPlayerRepeatSubscription: any;

  constructor(private _musicPlayerService: MusicPlayerService, private _element: ElementRef) {}

  ngOnInit() {
    this.repeat = this._musicPlayerService.getRepeatStatus();
    this.highlight();

    // Subscribe for repeat changes to update bindings
    this._musicPlayerRepeatSubscription = this._musicPlayerService.musicPlayerRepeatEventEmitter.subscribe(
      (event: any) => {
        this.repeat = event.data;
        this.highlight();
      }
    );
  }

  ngOnDestroy() {
    this._musicPlayerRepeatSubscription.unsubscribe();
  }

  /**
   * Element click event handler
   */
  @HostListener('click', ['$event'])
  onClick() {
    this._musicPlayerService.repeatToggle();
  }

  /**
   * Change background image of element based on repeat state
   */
  private highlight(): void {
    this._element.nativeElement.style.backgroundImage = this.repeat
      ? 'url("/assets/img/icons/repeat-red.svg")'
      : 'url("/assets/img/icons/repeat-gris-fonce.svg")';
  }
}
