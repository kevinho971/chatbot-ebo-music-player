export interface Album {
  id: string;
  title: string;
}

export interface Artist {
  id: string;
  name: string;
}

export interface Media {
  id: string;
  title: string;
  duration: number;
  album: Album;
  artist: Artist;
}

export interface AssoMediaPlaylist {
  id: string;
  displayOrder: number;
  media: Media;
}

export interface Playlist {
  id: string;
  title: string;
  assoMediaPlaylist: AssoMediaPlaylist[];
}

export interface PlaylistInterface {
  id: string;
  name: string;
  shareItemUrl: string;
  description: string;
  playlist: Playlist;
  privateShareItem: boolean;
}
