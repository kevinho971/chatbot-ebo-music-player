import { Component, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import { PlaylistService } from '../../services/playlist/playlist.service';
import { MusicPlayerService } from 'ngx-soundmanager2plus';
import { DataService } from '../../services/data/data.service';
import { ShareItemModel } from '../../services/playlist/shareItem.model';
import { environment } from '../../../environments/environment';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { SnackbarService } from '../../services/snackbar.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'ebo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  expand = false;
  songs: any[];
  shareItemInterface: ShareItemModel;

  state: boolean;
  currentPlaying: any;
  currentTrackPostion: number;
  currentTrackDuration: number;
  currentTrackProgress: number;

  previousButton;
  private _musicPlayerTrackIdSubscription: any;
  artwork: string;
  fixed: boolean;

  @HostListener('window:scroll', [])
  checkScroll() {
    const scrollPosition = window.pageYOffset;
    this.fixed = scrollPosition >= 450;
  }

  constructor(
    private playlistService: PlaylistService,
    private _musicPlayerService: MusicPlayerService,
    private data: DataService,
    @Inject(DOCUMENT) private document,
    private titleService: Title,
    private _snackBarService: SnackbarService
  ) {}

  ngOnInit() {
    this.getShareItem();
    this.currentPlaying = this._musicPlayerService.currentTrackData();

    setInterval(() => {
      this.setTitle();
      this.checkIfFirstSong();
    }, 1000);
  }

  getShareItem() {
    //Push des infos medias dans l'objet songs
    const params = new HttpParams().set('host', 'summer.music-work.com');
    this.playlistService.getShareItem(params).subscribe(value => {
      this.shareItemInterface = value;
      this.artwork = environment.urlShareItem + '/shareitem/artwork/' + value.id;
      this.songs = [];
      for (let i = 0; i < this.shareItemInterface.playlist.assoMediaPlaylist.length; i++) {
        this.songs.push({
          id: this.shareItemInterface.playlist.assoMediaPlaylist[i].media.id,
          title: this.shareItemInterface.playlist.assoMediaPlaylist[i].media.title,
          artist: this.shareItemInterface.playlist.assoMediaPlaylist[i].media.artist.name,
          duration: this.shareItemInterface.playlist.assoMediaPlaylist[i].media.duration,
          cover:
            environment.urlShareItem +
            '/shareitem/media/artwork/' +
            this.shareItemInterface.playlist.assoMediaPlaylist[i].media.id,
          url: environment.urlShareItem + '/shareitem/media/' + this.shareItemInterface.playlist.assoMediaPlaylist[i].media.id,
        });
      }
      this.setPlaylist();
    });

    //Listener position track
    this._musicPlayerTrackIdSubscription = this._musicPlayerService.musicPlayerTrackEventEmitter.subscribe(
      (event: any) => {
        this.currentPlaying = this._musicPlayerService.currentTrackData();
        this.currentTrackPostion = event.data.trackPosition;
        this.currentTrackDuration = event.data.trackDuration;
        this.currentTrackProgress = event.data.trackProgress;
      }
    );
  }


  checkIfFirstSong() {
    if (this.state) {
      this.previousButton = document.querySelector('button.previous');
      if (this.currentPlaying !== this.songs[0]) {
        if (this.previousButton) {
          this.previousButton.removeAttribute('disabled', '');
        }
      }
    }
  }

  setPlaylist() {
    setTimeout(() => {
      for (let i = 0; i < this.songs.length; i++) {
        this._musicPlayerService.addTrack(this.songs[i]);
      }
      //Statut de lecture des medias
      this.data.currentPlay.subscribe(state => (this.state = state));
    }, 1500);
  }

  setTitle() {
    if (this.currentPlaying)
      this.titleService.setTitle(this.currentPlaying.title + ' - ' + this.currentPlaying.artist + ' | Ebo Music Player');
  }

  ngOnDestroy() {
    this._musicPlayerTrackIdSubscription.unsubscribe();
  }
}
