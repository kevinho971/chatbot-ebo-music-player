import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private _translateService: TranslateService) { }

  getLanguage() {
    return this._translateService.getBrowserLang();
  }
}
