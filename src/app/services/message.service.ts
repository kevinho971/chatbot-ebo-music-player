import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { MusicPlayerService } from 'ngx-soundmanager2plus';
import { WitResponse } from '../models/witResponse.interface';
import { DataService } from './data/data.service';

export class Message {
  constructor(public content: string, public sentBy: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  conversation = new BehaviorSubject<Message[]>([]);
  private intent: string;
  private isShuffle: boolean;
  private isRepeat: boolean;

  constructor(private http: HttpClient,
              private _musicPlayerService: MusicPlayerService,
              private data: DataService) { }

  sendUtterance(params: HttpParams) {
    return this.http.get<WitResponse>('https://api.wit.ai/message?' + this.formatDate(), {params});
  }

  formatDate() {
    const d = new Date();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('');
  }


  update(msg: Message) {
    this.conversation.next([msg]);
  }

  converse(msg: string) {
    const userMsg = new Message(msg, 'user');
    this.update(userMsg);

    const params = new HttpParams().set('q', msg)
    this.sendUtterance(params).subscribe(res => {
      console.log(res);
      if (res.intents.length > 0)
        this.intent = res.intents[0].name;
      else if (res.traits){
        const trait = Object.keys(res.traits);
        this.intent = trait[0];
      }
      this.handleResponseFromWit();
    });
  }

  handleResponseFromWit() {
    const botMsg = new Message('', 'bot');
    switch (this.intent) {
      case 'wit$play_music':
        botMsg.content = 'Start playing';
        this.data.changePlay(true);
        this._musicPlayerService.play();
        this.update(botMsg);
        break;
      case 'wit$play':
        botMsg.content = 'Start playing';
        this.data.changePlay(true);
        this._musicPlayerService.play();
        this.update(botMsg);
        break;
      case 'wit$resume':
        botMsg.content = 'Resume music';
        this.data.changePlay(true);
        this._musicPlayerService.play();
        this.update(botMsg);
        break;
      case 'wit$resume_music':
        botMsg.content = 'Resume music';
        this.data.changePlay(true);
        this._musicPlayerService.play();
        this.update(botMsg);
        break;
      case 'wit$pause':
        botMsg.content = 'The song is on pause';
        this.data.changePlay(false);
        this._musicPlayerService.pause();
        this.update(botMsg);
        break;
      case 'wit$pause_music':
        botMsg.content = 'The song is on pause';
        this.data.changePlay(false);
        this._musicPlayerService.pause();
        this.update(botMsg);
        break;
      case 'wit$increase_volume':
        if (this._musicPlayerService.getVolume() < 100) {
          this._musicPlayerService.adjustVolume(true)
          botMsg.content = 'The volume has been increased';
        } else {
          botMsg.content = 'The volume is at its maximum'
        }
        this.update(botMsg);
        break;
      case 'wit$decrease_volume':
        if (this._musicPlayerService.getVolume() > 0) {
          this._musicPlayerService.adjustVolume(false);
          botMsg.content = 'The volume has been decreased';
        } else {
          botMsg.content = 'The volume is at its minimum'
        }
        this.update(botMsg);
        break;
      case 'wit$previous_track':
        this._musicPlayerService.prevTrack();
        botMsg.content = 'Ooooh you liked the previous track ?! Listen it again'
        this.update(botMsg);
        break;
      case 'wit$skip_track':
        this._musicPlayerService.nextTrack();
        botMsg.content = 'Skip'
        this.update(botMsg);
        break;
      case 'wit$shuffle_playlist':
        this._musicPlayerService.shuffleToggle();
        this.isShuffle = this._musicPlayerService.getShuffleStatus();
        this.data.changeShuffle(this.isShuffle);
        botMsg.content = 'Everyday i\'m shuffling';
        this.update(botMsg);
        break;
      case 'wit$unshuffle_playlist':
        this._musicPlayerService.shuffleToggle();
        this.isShuffle = this._musicPlayerService.getShuffleStatus();
        this.data.changeShuffle(this.isShuffle);
        botMsg.content = 'Everyday i\'m shuffling';
        this.update(botMsg);
        break;
      case 'wit$stop_music':
        this._musicPlayerService.stop();
        this.data.changePlay(false);
        botMsg.content = 'Oooooh you\'re leaving ??';
        this.update(botMsg);
        break;
      case 'wit$stop':
        this._musicPlayerService.stop();
        this.data.changePlay(false);
        botMsg.content = 'Oooooh you\'re leaving ??';
        this.update(botMsg);
        break;
      case 'wit$greetings':
        botMsg.content = 'Hello if you wanna listen some music just tell me ;)';
        this.update(botMsg);
        break;
      case 'wit$bye':
        botMsg.content = 'Bye bye !';
        this.data.changePlay(false);
        this._musicPlayerService.stop();
        this.update(botMsg);
        break;
      case 'mute':
        botMsg.content = 'Shhhhhhhh silence please';
        this._musicPlayerService.mute();
        this.update(botMsg);
        break;
      case 'wit$loop_music':
        botMsg.content = 'Looping the playlist';
        this._musicPlayerService.repeatToggle();
        this.isRepeat = this._musicPlayerService.getRepeatStatus();
        this.data.changeRepeat(this.isRepeat);
        this.update(botMsg);
        break;
      case 'wit$unloop_music':
        botMsg.content = 'Looping the playlist';
        this._musicPlayerService.repeatToggle();
        this.isRepeat = this._musicPlayerService.getRepeatStatus();
        this.data.changeRepeat(this.isRepeat);
        this.update(botMsg);
        break;
      default:
        botMsg.content = 'Can you precise your demands ?'
        this.update(botMsg);
    }
  }
}
