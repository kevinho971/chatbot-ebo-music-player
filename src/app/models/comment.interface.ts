export enum CommentType {
  LIKE = "LIKE", DISLIKE = "DISLIKE"
}

export interface CommentInterface {
  id: string,
  comment: string,
  commentType: CommentType,
  email: string,
  media: {
    id: string
  },
  shareItem: {
    id: string
  }
}
