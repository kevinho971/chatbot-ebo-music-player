import { Directive, ElementRef, HostListener, OnInit, OnDestroy } from '@angular/core';
import { MusicPlayerService } from 'ngx-soundmanager2plus';

@Directive({
  selector: '[appShuffle]'
})
export class ShuffleDirective implements OnInit, OnDestroy {
  shuffle: boolean;

  // subscriptions
  private _musicPlayerShuffleSubscription: any;

  constructor(private _musicPlayerService: MusicPlayerService, private _element: ElementRef) {}

  ngOnInit() {
    this.shuffle = this._musicPlayerService.getRepeatStatus();
    this.highlight();

    // Subscribe for repeat changes to update bindings
    this._musicPlayerShuffleSubscription = this._musicPlayerService.musicPlayerShuffleEventEmitter.subscribe(
      (event: any) => {
        this.shuffle = event.data;
        this.highlight();
      }
    );
  }

  ngOnDestroy() {
    this._musicPlayerShuffleSubscription.unsubscribe();
  }

  /**
   * Element click event handler
   */
  @HostListener('click', ['$event'])
  onClick() {
    this._musicPlayerService.shuffleToggle();
  }

  /**
   * Change background color of element based on repeat state
   */
  private highlight(): void {
    this._element.nativeElement.style.backgroundImage = this.shuffle
      ? 'url("/assets/img/icons/shuffler-red.svg")'
      : 'url("/assets/img/icons/shuffler-gris-fonce.svg")';
  }
}
