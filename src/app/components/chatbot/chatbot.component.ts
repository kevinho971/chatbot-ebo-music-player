import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpParams} from "@angular/common/http";
import { MessageService, Message } from '../../services/message.service';
import { Observable } from 'rxjs/internal/Observable';
import { scan } from 'rxjs/operators';


@Component({
  selector: 'ebo-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {
  form!: FormGroup;
  toggle = true;

  messages: Observable<Message[]>

  constructor(private _messageService: MessageService,
              private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.messages = this._messageService.conversation.asObservable().pipe(
      scan((acc, value) => acc.concat(value))
    );
  }

  toggleChat() {
    this.toggle = !this.toggle;
  }

  initForm() {
    this.form = this._formBuilder.group({
      message: [null, Validators.required]
    });
  }

  // sendUtterance() {
  //   const params = new HttpParams()
  //     .set('q', this.form.value['message']);
  //   this._messageService.sendUtterance(params).subscribe((res: any) => {
  //     console.log(res);
  //   })
  // }

  sendMessage() {
    this._messageService.converse(this.form.value['message']);
    this.form.reset();
  }

}
